#uncomment and locally adapt next line to fix the full helpfile path name
HELPFILENAME = -DHELPFILENAME=\"/usr/share/njplot/njplot.help\"

#comment out next line and uncomment next 2 to use the PDFLib Lite library 
NO_PDF = -DNO_PDF
#PDF = ../PDFlibLite/libs/pdflib
#PDFLIB = -L$(PDF) -lpdf

# c compiler and linker
CC = gcc

# Vibrant top directory
VIBRANT = /usr/include/ncbi

OBJECTS = njplot-vib.o
OBJUNROOTED = unrooted-vib.o preptree.o


CFLAGS  = -c -DWIN_MOTIF -Dunix -I$(VIBRANT) $(HELPFILENAME) $(NO_PDF)
	
all: njplot  unrooted newicktops newicktotxt


njplot : $(OBJECTS)
	$(CC) -o njplot $(OBJECTS) \
        -lvibrant -lncbi -lXm -lXt
        
unrooted : $(OBJUNROOTED)
	$(CC) -o unrooted $(OBJUNROOTED) \
        -lvibrant -lncbi -lXm -lXt

newicktops: njplot-vib.c
	$(CC) -DNO_GUI  -DNO_PDF -o $@ njplot-vib.c -lm

newicktotxt: njplot-vib.c
	$(CC) -DTTY  -o $@ njplot-vib.c -lm


.c.o : 
	$(CC) $(CFLAGS)  $?

clean:
	rm -f *.o

distclean: clean
	rm -f njplot unrooted newicktops newicktotxt
